from PIL import Image
import os

image_dir = './128/samples'
new_image_dir = './images/png'

fs = os.listdir(image_dir)
print(len(fs))
for fn in fs:
    im = Image.open('%s/%s' % (image_dir, fn))
    if fn != '.DS_Store':
        im.save('%s/%s' % (new_image_dir, fn.replace('.jpg', '.png')))
        print('%s/%s' % (new_image_dir, fn.replace('.jpg', '.png')))
