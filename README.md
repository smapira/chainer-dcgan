# DCGAN

    https://github.com/chainer/chainer/tree/master/examples/dcgan

## Flow

### Prepare

```bash
# download
python3 google_images_download.py -k "子 干支 文字"

# convert
mkdir 128/samples samples
cp -pr google-images-download/google_images_download/downloads/**/* samples/.
python resize.py 
python decolor.py
```

### colab

upload images google drive
open chainer-dcgan-greeting.ipynb

```bash
%run train_dcgan.py --gpu 0 --dataset images --snapshot_interval 500 --display_interval 100 --epoch 3000
%run train_dcgan.py --gpu 0 --resume result/snapshot_iter_6500.npz --dataset images --snapshot_interval 500 --display_interval 100 --epoch 3000
```

### ?

    generator.py --gen result/gen_iter_24500.npz --out dist
        
