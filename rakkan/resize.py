import os
import glob
import sys
import traceback
from PIL import Image
from pathlib import Path

image_dir = sys.argv[1] + '/**/*'
files = glob.glob(image_dir)
SIZE = 224

for f in files:
    try:
        title, ext = os.path.splitext(f)
        print(str(SIZE) + "/" + title + ext)
        Path(str(SIZE) + "/" + os.path.dirname(f)).mkdir(parents=True, exist_ok=True)

        img = Image.open(f).convert('RGB')
        w, h = img.size
        new_height = int(SIZE * h / w)
        new_width = int(SIZE * w / h)
        img = img.resize((new_width, new_height), Image.ANTIALIAS)

        img.save(str(SIZE) + "/" + title + ext)

    except Exception as e:
        print("Exception in user code:")
        print('-' * 60)
        traceback.print_exc(file=sys.stdout)
        print('-' * 60)
    finally:
        pass
