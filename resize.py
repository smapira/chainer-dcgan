import os
import glob
from PIL import Image
import sys, traceback
image_dir = 'samples/*'
files = glob.glob(image_dir)
SIZE = 128

for f in files:
    try:
        img = Image.open(f).convert('RGB')
        img_resize = img.resize((SIZE, SIZE))
        ftitle, fext = os.path.splitext(f)
        print(str(SIZE) + "/" + ftitle + fext)
        img_resize.save(str(SIZE) + "/" + ftitle + fext)
    except Exception as e:
        print
        "Exception in user code:"
        print
        '-' * 60
        traceback.print_exc(file=sys.stdout)
        print
        '-' * 60
    finally:  # Optional
        pass  # Clean up
