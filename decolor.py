import sys
import cv2
import os

image_dir = './images/png'
new_image_dir = './images/decolored'
new_image_dir2 = './images/decolored2'

fs = os.listdir(image_dir)
print(len(fs))
for fn in fs:
    # print('%s/%s' % (image_dir, fn))
    if fn == '.DS_Store':
        continue

    # img = cv2.imread('%s/%s' % (image_dir, fn))
    # img_gray, _ = cv2.decolor(img)
    # cv2.imwrite('%s/%s' % (new_image_dir, fn), img_gray)


    img_bgr = cv2.imread('%s/%s' % (image_dir, fn))
    img_gray = cv2.cvtColor(img_bgr, cv2.COLOR_BGR2GRAY)
    cv2.imwrite('%s/%s' % (new_image_dir2, fn), img_gray)
